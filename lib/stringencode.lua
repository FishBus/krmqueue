local prefix = "."

local function export_item_with_tags(data, tagname)
	if not tagname then
		tagname = "extdata"
	end
	local result = nil
	local item = game.surfaces[1].create_entity({
		name="item-on-ground",
		position={0,0},
		stack='item-with-tags',
	})
	item.stack.set_tag(tagname, data)
	result = item.stack.export_stack()
	item.destroy()
	return result
end

local function import_item_with_tags(luadata, tagname)
	if not tagname then
		tagname = "extdata"
	end
	local result = nil
	local item = game.surfaces[1].create_entity({
		name="item-on-ground",
		position={0,0},
		stack='item-with-tags',
	})
	item.stack.import_stack(data)
	result = item.stack.get_tag(tagname)
	item.destroy()
	return result
end

local function import_string_by(data, methodid)
	local outdata = nil
	if methodid == "0" then
		outdata = import_item_with_tags(data, "ext")
	end
	if methodid == "1" then
		outdata = game.json_to_table(data)
	end
	return outdata
end

function export_string(data, methodid)
	local outstr = ""
	if not methodid then
		methodid = "0"
	end
	if methodid == "0" then
		outstr = export_item_with_tags(data, "ext")
	end
	if methodid == "1" then
		oustring = game.table_to_json(data)
	end
	
	return prefix..methodid..outstr
end

function import_string(data)
	local marker = string.sub(data, 1, 1)
	local result = nil
	if marker == prefix then
		local method = string.sub(data, 2, 1)
		local datastr = string.sub(data, 3)
		result = import_string_by(datastr, method)
	elseif marker == "{" then
		result = game.json_to_table(data)
	else
		result = import_item_with_tags(data)
	end
	return result
end

return {
	export = export_string,
	import = import_string,
}
