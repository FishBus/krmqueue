# krmqueue

Kovus' RCON Message Queue

Message Queue for Factorio RCON connections.  Serves as a dependency for multiple RCON message-queue systems.